/* frameworks */
//=include ../../dist/bower_components/jquery/dist/jquery.js
//=include ../../dist/bower_components/moff/dist/moff.min.js

/* libs */
//=include lib/modernizr-custom.js
//=include ../../dist/bower_components/bootstrap/js/dist/util.js

/* plugins */

/* separate */
//=include helpers/object-fit.js
//=include separate/global.js

/* components */
// components/js-header.js

// the main code

window.moffConfig = {
	// Website CSS breakpoints.
	// Default values from Twitter Bootstrap.
	// No need to set xs, because of Mobile first approach.
	breakpoints: {
		xs: 480,
		sm: 768,
		md: 992,
		lg: 1200
	},
	loadOnHover: true,
	cacheLiveTime: 2000
};

Moff.amd.register({
	id: 'header',
	// depend: {
	// 	js: ['http://chat-domain.com/js/chat-api.js'],
	// 	css: ['http://chat-domain.com/css/chat.css']
	// },
	file: {
		js: ['s/js/components/js-header.js']
	},

	beforeInclude: function() {},
    afterInclude: function() {},
    
	loadOnScreen: ['xs', 'sm', 'md', 'lg'],
	onWindowLoad: true
});

if($(".js-map").length > 0){
    Moff.amd.register({
        id: 'myMapId',
        file: {
            js: ['./s/js/components/js-map.js']
        },

        beforeInclude: function() {},
        afterInclude: function() {},

        loadOnScreen: ['xs', 'sm', 'md', 'lg'],
        onWindowLoad: true
    });
}

if($(".modal").length > 0){
    Moff.amd.register({
        id: 'modal',
        depend: {
            js: ['s/js/plugins/bs/modal.js']
        },
        file: {
            js: ['s/js/helpers/bs_modal_fix.js']
        },

        beforeInclude: function() {},
        afterInclude: function() {},

        loadOnScreen: ['xs', 'sm', 'md', 'lg'],
        onWindowLoad: true
    });
}

if($(".scene").length > 0){
    Moff.amd.register({
        id: 'parallax',
        depend: {
            js: ['https://cdnjs.cloudflare.com/ajax/libs/parallax/3.1.0/parallax.min.js']
        },
        file: {
            js: ['s/js/components/js-parallax.js']
        },

        beforeInclude: function() {},
        afterInclude: function() {},

        loadOnScreen: ['xs', 'sm', 'md', 'lg'],
        onWindowLoad: true
    });
}