(function(){

    ymaps.ready(init);

    var myMap = true;

    function init () {
        var myMap = new ymaps.Map('my-map-id', {
                center: [57.631074, 39.870421],
                zoom: 17
            }, {
                searchControlProvider: 'yandex#search'
            }),
            objectManager = new ymaps.ObjectManager({
                // Чтобы метки начали кластеризоваться, выставляем опцию.
                clusterize: true,
                // ObjectManager принимает те же опции, что и кластеризатор.
                gridSize: 32,
                searchControlProvider: 'yandex#search',
                clusterDisableClickZoom: true
            });

        $(".js-left-menu-btn").on("click", function () {
            $(myMap.container.getParentElement()).toggleClass('c-contacts__map-container-flexible');
        });
        $(".js-left-menu-btn").on("click", function () {
            setTimeout(function() {
                myMap.container.fitToViewport();
            }, 400);
        });

        myMap.geoObjects
        .add(objectManager)
        .add(new ymaps.Placemark([57.631074, 39.870421], {
            
            iconCaption: 'улица Победы,37'
        }, {
            preset: 'islands#blueCircleDotIconWithCaption',
            iconCaptionMaxWidth: '120'
        }))
    }
    

        // Чтобы задать опции одиночным объектам и кластерам,
        // обратимся к дочерним коллекциям ObjectManager.
        // objectManager.objects.options.set('preset', 'islands#redIcon');
        // objectManager.clusters.options.set('preset', 'islands#redClusterIcons');
        // myMap.geoObjects.add(objectManager);
        // $.ajax({
        //     url: "s/data/map-coordinates.json"
        // }).done(function(data) {
        //     objectManager.add(data);
        // });
    // }

})();