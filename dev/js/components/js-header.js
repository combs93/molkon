(function(){
    function onScroll() {
        if ($(window).scrollTop() > 70) {
            $('.c-header').addClass('sticky');
        } else {
            $('.c-header').removeClass('sticky');
        }
        $('.c-header__contacts-item').each(function() {
        	var scrollTop = $(document).scrollTop();
            var hash = $(this).attr('href'),
                target = $(hash);
            if (target.position().top <= scrollTop &&  target.position().top + target.outerHeight() > scrollTop) {
                $('.c-header__contacts-item.active').removeClass('active');
                $(this).addClass('active');
            } else {
                $(this).removeClass('active');
            }
        });
    }

    onScroll();
    $(document).on('scroll', onScroll);

    $('.c-header__hamburger').on('click', function() {
	    $('.c-header__menu-responsive').animate({
	        right: '0px'
	    }, 250);

	    $('body').animate({
	        right: '270px'
	    }, 250);
	    $('body').addClass('faded');
	    $('.c-header').addClass('responsive');
	});

	$('.c-header__menu-close .icon').on('click', function() {
	    $('.c-header__menu-responsive').animate({
	        right: '-270px'
	    }, 250);

	    $('body').animate({
	        right: '0px'
	    }, 250);
	    $('body').removeClass('faded');
	    $('.c-header').removeClass('responsive');
	});

    if(window.innerWidth < 992) {
        $('.c-header__call-back-item.call-back').on('click', function() {
            $('.c-header__menu-close .icon').trigger('click');
        });
    }

	$('.c-header__contacts-item').on('click', function(e) {
        e.preventDefault();
        var idSection = $(this).attr('href'),
            section = $(idSection).offset().top - 85;
        $('body, html').animate({scrollTop: section}, 1500);
        if($('.c-header__contacts-item').hasClass('active')) {
            setTimeout(function() {
                 $('.c-header__menu-close .icon').trigger('click');
            }, 1600)
        }
	});

})();